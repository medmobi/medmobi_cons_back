CREATE TABLE usuario(
	id			SERIAL NOT NULL PRIMARY KEY,
    nome 		VARCHAR(50) NOT NULL,
    email 		VARCHAR(20),
    data_nasc 	TIMESTAMP,
    sexo		CHAR,
    celular 	VARCHAR(20),
    telefone	VARCHAR(20),
    avatar		BYTEA,
    tipo 		INT,
    
    login 		VARCHAR(50) NOT NULL,
    senha		TEXT NOT NULL
);

CREATE TABLE chamada(
	id 					SERIAL NOT NULL PRIMARY KEY,
    data_hora_inicio	TIMESTAMP NOT NULL,
    data_hora_fim		TIMESTAMP,
    duracao				TIMESTAMP,
    tipo_chamada		INT,
    status				INT
);

CREATE TABLE usuario_chamada(
	usuario_id INT,
    chamada_id INT,
    
    PRIMARY KEY(usuario_id, chamada_id),
    FOREIGN KEY(usuario_id) REFERENCES usuario(id),
    FOREIGN KEY(chamada_id) REFERENCES chamada(id)
);

CREATE TABLE arquivo(
    id 				SERIAL NOT NULL PRIMARY KEY,
    chamada_id		INT NOT NULL,
    arquivo			BYTEA,
    tipo_arquivo	INT NOT NULL,
    data_hora		TIMESTAMP,
    
    FOREIGN KEY(chamada_id) REFERENCES chamada(id)
);

CREATE TABLE mensagem(
	id 				SERIAL NOT NULL PRIMARY KEY,
    remetente_id	INT NOT NULL,
    chamada_id		INT NOT NULL,
    mensagem		TEXT,
    data_hora_envio	TIMESTAMP,
    
    FOREIGN KEY(remetente_id) REFERENCES usuario(id),
    FOREIGN KEY(chamada_id) REFERENCES chamada(id)
);

CREATE TABLE recebido(
	remetente_id 		INT NOT NULL,
    mensagem_id 		INT NOT NULL,
    chamada_id			INT NOT NULL,
    destinatario_id		INT NOT NULL,
    data_hora_entrega 	TIMESTAMP,
    data_hora_envio		TIMESTAMP,
    
    FOREIGN KEY(remetente_id) REFERENCES usuario(id),
    FOREIGN KEY(mensagem_id) REFERENCES mensagem(id),
    FOREIGN KEY(chamada_id) REFERENCES chamada(id),
    FOREIGN KEY(destinatario_id) REFERENCES usuario(id)
);

CREATE TABLE exame(
	id 				SERIAL NOT NULL PRIMARY KEY,
    usuario_id		INT NOT NULL,
    arquivo			BYTEA,
    descricao		VARCHAR(200),
    data_hora		TIMESTAMP,
   
    FOREIGN KEY(usuario_id) REFERENCES usuario(id)
);

CREATE TABLE alteracao_exame(
	id 				SERIAL NOT NULL PRIMARY KEY,
    exame_id		INT NOT NULL,
    usuario_id		INT NOT NULL,
    arquivo			BYTEA,
    data_hora		TIMESTAMP,
    
    FOREIGN KEY(exame_id) REFERENCES exame(id),
    FOREIGN KEY(usuario_id) REFERENCES usuario(id)
);