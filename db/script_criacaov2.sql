CREATE TABLE IF NOT EXISTS "User"
(
	"id"	 		SERIAL NOT NULL PRIMARY KEY,
	nome  			VARCHAR(200) NOT NULL,
	"admin" 		BOOLEAN,
	realm			VARCHAR(200),
	username		VARCHAR(50),
	challenges		VARCHAR(200),
	email			VARCHAR(50),
	emailVerified	BOOLEAN,
	status			BOOLEAN,
	created			TIMESTAMP,
	lastUpdated		TIMESTAMP,
	pusu_id			INT,
	pres_id			INT,
	emp_id			INT,
	credentials		VARCHAR(200)
)

CREATE TABLE Paciente(
	id 					SERIAL NOT NULL PRIMARY KEY,
	foto 				VARCHAR[200],
	sexo				VARCHAR[1],
	data_nascimento		TIMESTAMP,
	obs					VARCHAR[200],
	nome_fonetico		VARCHAR[200],
	profissao			VARCHAR[200],
	nome_mae			VARCHAR[200],
	nome_pai			VARCHAR[200],
	profissao_mae		VARCHAR[200],
	profissao_pai		VARCHAR[200],
	como_conheceu		VARCHAR[200],
	descricao_indicacao	VARCHAR[200],
	rg					VARCHAR[20],
	rg_orgao_emissor	VARCHAR[2],
	rg_data_emissao		TIMESTAMP,
	registro_nascimento	TIMESTAMP,
	cns 				VARCHAR[100],
	nome 				VARCHAR[200],
	email 				VARCHAR[50],
	ec_id				INT,
	pais_id				INT,
	uf_id				INT,
	cid_id				INT,
	ts_id				INT,
	createdAt			TIMESTAMP,
	updatedAt			TIMESTAMP,
	emp_id				INT

);

