CREATE TABLE usuario(
	id serial not null,
    nome character varying not null,
    admin boolean,
    realm varchar(200),
    username varchar(50),
    challenges varchar(200),
    email varchar not null,
    emailverified boolean,
    status boolean,
    created timestamp without time zone,
    lastupdated timestamp without time zone,
    pusu_id integer,
    pres_id	integer,
    emp_id integer,
    credentials varchar,
    api_paciente_id integer,
    api_user_id integer,
    password character varying not null,
    verificationtoken vcharacter varying
);

CREATE TABLE sala(
    id character varying not null primary key,
    createdAt timestamp,
    updatedAt timestamp,
    criador_id  integer,
    paciente_id integer,
    duracao timestamp,
    foreign key(criador_id) references usuario(id),
    foreign key(paciente_id) references usuario(id)
);
ALTER TABLE sala ALTER COLUMN duracao TYPE character varying;
ALTER TABLE sala ALTER COLUMN createdat TYPE character varying;
ALTER TABLE sala ALTER COLUMN updatedat TYPE character varying;

ALTER TABLE sala ADD COLUMN retorno BOOLEAN NOT NULL DEFAULT FALSE;
ALTER TABLE sala ADD COLUMN data_prevista character varying;
ALTER TABLE sala ADD COLUMN data_envio_email character varying;

CREATE TABLE video(
    id serial not null primary key,
    descricao character varying not null,
    file_id character varying not null,
    container_id character varying not null,
    sala_id integer not null,
    createdAt character varying,
    foreign key(sala_id) references sala(id)
)

CREATE TABLE video_participante(
    id serial not null primary key,
    video_id integer not null,
    sala_id integer not null,
    participante_id integer not null
    foreign key(video_id) references video(id),
    foreign key(sala_id) references sala(id),
    foreign key(participante_id) references participante(id)    

) 

CREATE TABLE participante(
   id serial not null primary key,
   sala_id varchar[200] not null,
   usuario_id integer not null,
   createdAt timestamp,
   endedAt timestamp ,
   foreign key(sala_id) references sala(id),
   foreign key(usuario_id) references usuario(id)
);
    
ALTER TABLE participante ADD COLUMN ativo boolean NOT NULL DEFAULT TRUE;

ALTER TABLE participante ALTER COLUMN createdat TYPE character varying;

ALTER TABLE participante ALTER COLUMN endedat TYPE character varying;


CREATE TABLE mensagem(
    id serial not null primary key,
    sala_id character varying not null,
    remetente_id integer not null,
    data_hora_envio timestamp,
    mensagem character varying,
    foreign key(sala_id) references sala(id),
    foreign key(remetente_id) references usuario(id)
);
ALTER TABLE mensagem ALTER COLUMN data_hora_envio TYPE character varying;


CREATE TABLE controle_mensagem(
    id serial not null primary key,
    sala_id character varying not null,
    mensagem_id integer not null,
    destinatario_id integer not null,
    acao integer,
    data_hora character varying,
    foreign key(sala_id) references sala(id),
    foreign key(mensagem_id) references mensagem(id),
    foreign key(destinatario_id) references usuario(id)
)
