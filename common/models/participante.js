'use strict';

module.exports = function(Participante) {

	  Participante.remover = function(usuario, sala, cb){
		Participante.findOne({where: {and: [{sala_id: {ilike: sala}},{usuario_id: usuario}]}}, function(err, results){
			results.updateAttributes({"ativo": false, "endedat": new Date()}, function(err, r){
				cb(null, r);
			});
		});
 	 };

	  Participante.remoteMethod('remover', {
	        accepts: [
	        {	
	            arg: 'usuario',
	            type: 'number'
	        },
	        {
	            arg: 'sala',
	            type: 'string'
	        }],
	        returns: {
	            arg: 'participante',
	            type: 'string'
	        },
	        http: {
	            path: '/remover',
	            verb: 'post'
	        }
	    });
	
};
