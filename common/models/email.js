'use strict';

const EMAIL = "envio@medmobi.com.br";
const LINK = "http://aol.medmobi.tk";

module.exports = function(Email) {
	Email.notificaChamada = function(destinatario, sala_id, cb) {
    var mail = Email.app.models.email.send({
      to: destinatario,
      from: EMAIL,
      subject: 'MedMobi - Atendimento Online',
      text: 'MedMobi - Atendimento Online',
      html: '<p>Para acessar a sala do atendimento online, clique no <a href="' + LINK + '/#/sala/'+ sala_id +'">link</a>.</p>'
    }, function(err, mail) {
         console.log('email sent!');
         if(cb){
            cb(err, mail);
         }
    });
  };


   Email.remoteMethod('notificaChamada', {
        accepts: [
        {
            arg: 'destinatario',
            type: 'string'
        },
        {
            arg: 'sala_id',
            type: 'string',
        }],
        http: {
            path: '/notificaChamada',
            verb: 'post'
        }
    });

   Email.notificaRetorno = function(paciente, paciente_id, sala_id, cb) {
    Email.app.models.email.send({
      to: paciente,
      from: EMAIL,
      subject: 'MedMobi - Atendimento Online',
      html: '<p>Para acessar o vídeo de retorno do atendimento, clique no <a href="'+ LINK +'/#/retorno/'+ paciente_id +'/' + sala_id + '">link</a>.</p>',
    }, function(err, mail) {
          cb(err, mail);
          Email.app.models.sala.findOne({"where": {"id": sala_id}}, function(err, sala, cb){
          if(sala != null){
             sala.updateAttributes({"data_envio_email": new Date(), "updatedat": new Date()}, function(err, result){
                if(err!= null){
                  console.log("Erro ao atualizar sala! " + err);
                }
             });
          }
        });
      // }
    });
  };


   Email.remoteMethod('notificaRetorno', {
        accepts: [
        {
            arg: 'paciente',
            type: 'string'
        },
        {
            arg: 'paciente_id',
            type: 'number'
        },
        {
            arg: 'sala_id',
            type: 'string',
        }],
        returns: {
            arg: 'sala',
            type: 'Sala'
        },
        http: {
            path: '/notificaRetorno',
            verb: 'post'
        }
    });
 };
