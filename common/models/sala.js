'use strict';

module.exports = function(Sala) {
  /**
 * Open new room
 * @param {number} medico_id Id do medico
 * @param {number} paciente_id Id do paciente
 * @param {Function(Error, string)} callback
 */

  Sala.agendar = function(criador_id, paciente_id, retorno, data_prevista,evento_id, cb){

    const uuidv1 = require('uuid/v1');
    var id = uuidv1();
    id = id.replace(/-/g,"");
    Sala.create({"id": id, "criador_id": criador_id, "paciente_id": paciente_id, "retorno": retorno, "data_prevista": data_prevista,'evento_id':evento_id}, function(err, sala){
    var sala = sala;
    Sala.app.models.participante.create({"sala_id":id, "usuario_id":criador_id, "ativo":true}, function(err, participante_criador){
        var pc = participante_criador;
        Sala.app.models.participante.create({"sala_id":id, "usuario_id":paciente_id, "ativo":true}, function(err, participante_paciente){
            var pp = participante_paciente;
                cb(err, [sala, pc, pp]);
            });
        });
    });
  };

  Sala.remoteMethod('agendar', {
        accepts: [
        {
            arg: 'criador_id',
            type: 'number'
        },
        {
            arg: 'paciente_id',
            type: 'number'
        },
        {
            arg: 'data_prevista',
            type: 'string'
        },
        {
            arg: 'retorno',
            type: 'boolean',
        },
        {
            arg: 'evento_id',
            type: 'number',
        }
    ],
        returns: {
            arg: 'sala',
            type: 'Sala'
        },
        http: {
            path: '/agendar',
            verb: 'post'
        }
    });


  Sala.chamar = function(sala_id, criador_email, paciente_email, cb){
    Sala.app.models.email.notificaChamada(paciente_email, sala_id);
    Sala.app.models.email.notificaChamada(criador_email, sala_id);
    cb(null, sala_id) ;
  };

  Sala.remoteMethod('chamar', {
        accepts: [
        {
            arg: 'criador_id',
            type: 'number'
        },
        {
            arg: 'paciente_id',
            type: 'number'
        },
        {
            arg: 'paciente_email',
            type: 'string'
        },
        {
            arg: 'criador_email',
            type: 'string',
        },
        {
            arg: 'retorno',
            type: 'Boolean',
        },
        {
            arg: 'sala_id',
            type: 'string',
        }],
        returns: {
            arg: 'id',
            type: 'string'
        },
        http: {
            path: '/chamar',
            verb: 'post'
        }
    });

  Sala.notificar = function(sala_id, paciente_id, paciente_email, cb){
    Sala.app.models.email.notificaRetorno(paciente_email, paciente_id, sala_id);
    cb(null, sala_id);
  };

  Sala.remoteMethod('notificar', {
        accepts: [
        {
            arg: 'sala_id',
            type: 'string'
        },
        {
            arg: 'paciente_id',
            type: 'number'
        },
        {
            arg: 'paciente_email',
            type: 'string'
        }],
        returns: {
            arg: 'sala_id',
            type: 'string'
        },
        http: {
            path: '/notificar',
            verb: 'post'
        }
    });

   Sala.convidar = function(sala_id, participante_id, participante_email, cb){
    Sala.app.models.email.notificaChamada(participante_email, sala_id);
    cb(null, sala_id);
  };

  Sala.remoteMethod('convidar', {
        accepts: [
        {
            arg: 'sala_id',
            type: 'string'
        },
        {
            arg: 'participante_id',
            type: 'number'
        },
        {
            arg: 'participante_email',
            type: 'string'
        }],
        returns: {
            arg: 'sala_id',
            type: 'string'
        },
        http: {
            path: '/convidar',
            verb: 'post'
        }
    });

  Sala.encerrar = function(sala_id, cb){
    Sala.findById(sala_id, function(err, result){
    result.updateAttributes({"ativo": false, "updatedat": new Date(), "duracao": new Date() - result.createdat}, function(err, r){
        // if(result.retorno){
        //     Sala.app.models.prontuarios.adicionar_atendimento("Retorno - Atendimento Online", new Date(r.data_prevista), new Date(r.updatedat), r.duracao, 2, new Date(), null, r.paciente_id, r.criador_id, 1);
        // }else{
        //     Sala.app.models.prontuarios.adicionar_atendimento("Consulta - Atendimento Online", new Date(r.data_prevista), new Date(r.updatedat), r.duracao, 2, new Date(), null, r.paciente_id, r.criador_id, 1);
        // }
        // cb(null, r);
      cb(null, sala_id);
    });
   });
  };

  Sala.remoteMethod('encerrar', {
        accepts: [
        {
            arg: 'sala_id',
            type: 'string'
        }],
        returns: {
            arg: 'sala_id',
            type: 'string'
        },
        http: {
            path: '/encerrar',
            verb: 'post'
        }
    });

  Sala.consultar_historico_retorno = function(cb){
    Sala.find({where: {and: [{ativo: false},{retorno: true}]}}, function(err, result){
        cb(null, result);
   });
  };

  Sala.remoteMethod('consultar_historico_retorno', {
        returns: {
            arg: 'salas',
            type: 'string'
        },
        http: {
            path: '/consultar_historico_retono',
            verb: 'get'
        }
    });

  Sala.consultar_historico_consulta = function(cb){
    Sala.find({where: {and: [{ativo: false},{retorno: false}]}}, function(err, result){
        cb(null, result);
    });
  };

  Sala.remoteMethod('consultar_historico_consulta', {
        returns: {
            arg: 'salas',
            type: 'string'
        },
        http: {
            path: '/consultar_historico_consulta',
            verb: 'get'
        }
    });

};
