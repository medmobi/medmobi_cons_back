var path = require('path'),
fs = require("fs");
exports.privateKey = fs.readFileSync(path.join(__dirname, 'key.pem')).toString();
exports.certificate = fs.readFileSync(path.join(__dirname, 'cert.pem')).toString();